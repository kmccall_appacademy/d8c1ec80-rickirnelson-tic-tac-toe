require "board"

class Board
    attr_reader :grid, :board
    def grid
      @grid
    end

    def initialize(grid=[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
      @grid = grid
      #Array.new(3) { Array.new(3) }
    end

    # def [](pos)
    # x,y = [pos]
    # @grid[x][y]
    # end

    # def []=(pos,val)
    # x,y = pos
    # @grid[x][y] = val
    # end
    def place_mark(pos,mark)
      #self[pos] = mark
      @mark = mark
      @grid[pos[0]][pos[1]] = mark
      @pos = @grid[pos[0]][pos[1]]
    end

    def empty?(pos)
      @grid[pos[0]][pos[1]].nil? ? true : false
      #self[pos].nil? << call self, not grid
    end

    def winner
      #--rows
      return @mark if @grid.any? do |row|
        row.all? {|el| el == @mark}
      end
      #--columns
      return @mark if vertical(@grid).any? do |row|
        row.all? {|el| el == @mark}
      end
      #--diagonal down to right
      return @mark if diag_down(@grid).any? do |row|
        row.all? {|el| el == @mark}
      end
      #--diagonal up to right
      return @mark if diag_up(@grid).any? do |row|
        row.all? {|el| el == @mark}
      end

    # =begin
    #   [:X,:O].each { |mark| return mark id won?(mark)}
    # =end
    end

    # =begin
    #   def won?(mark)
    #     lines.any? do |line|
    #       line.all? { |pos| self[pos] == mark}
    #     end
    #   end
    # =end

    # =begin
    # def lines
    #   h_lines = (0..2).map do |y|
    #     [[0,y],[1,y],[2,y]]
    #   end
    #   v_lines = (0..2).map do |x|
    #     [[x,0],[x,1],[x,2]]
    #   end
    #   d_lines = [
    #     [[0,0],[1,1],[2,2]],
    #     [[2,0],[1,1],[0,2]]
    #   ]
    #   h_lines + v_lines + d_lines
    # =end
    def vertical(grid)
      arr = []
      @grid.each_with_index do |row,i|
        arr[i] = []
        @grid[i].each_with_index do |col,j|
           arr[i] << @grid[j][i]
        end
      end
      arr
    end

    def diag_down(grid)
      arr = []
      @grid.each_with_index do |row,i|
        arr[i] = []
        @grid[i].each_with_index do |col,j|
           arr[i] << @grid[j][j]
        end
      end
      arr
    end

    def diag_up(grid)
      arr = []
      @grid.each_with_index do |row,i|
        arr[i] = []
        @grid[i].each_with_index do |col,j|
           arr[i] << @grid.reverse[j][j]
        end
      end
      arr
    end

    def almost

    end
    def over?
      # =begin
      # !winner.nil? || @grid.flattern.count(nil) == 0
      # =end
      return true if winner
      return true if @grid.flatten.none?(&:nil?)
      return false if @grid.flatten.any?(&:nil?)
    end

    # def empty_positions
    #   positions.select {|pos| self.empty?(pos)}
    # end

    # def would_win?(pos,win)
      # place_mark(pos,mark)
      # would_win = winner == mark
      # self[pos] = nil
      # would_win
    # end
    #def positions
      # p = []
      # 3.times do |row|
      #   3.times {|col| p << [row,col]}
      # end
    #


    # def to_s #render
    #     @grid.map do |row|
    #       row.map{ |el| el.nil? ? "_" : el }.join(" ")
    #     end.join("\n")
    # end
# if __FILE___ = $PROGRAM_NAME
#   b = board.new
#   puts b.to_s
#   b.place.mark([0,1], :X)
#   b.place.mark([2,0], :O)
#


end
