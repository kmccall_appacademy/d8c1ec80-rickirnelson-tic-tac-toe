require "byebug"
require "game"

class ComputerPlayer
  attr_reader :comp, :board, :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end


  def display(board)
    @board = board
  end

  #def get_move
    #*add empty positions on board
    # @board.empty_positions.each do |pos|
    #   return pos if @board.would_win?(pos, @mark)
    # end
    # @board.empty_positions.sample
  #end
  def get_move
  #   debugger
    winning_arr = []
    random_arr = []
    oops = [[:O,nil,nil],[:O,nil,nil],[nil,nil,nil]]##how to get this to work?
    vertical(oops).each_with_index do |row,i|
     if row.count {|el| el != @mark} == 1
       vertical(oops)[i].each_with_index do |ch, j|
           winning_arr << [i,j] if ch != @mark
       end
     else row.count {|el| el != @mark} > 1
       vertical(oops)[i].each_with_index do |ch, j|
           random_arr << [i,j] if ch != :X && ch != :O
       end
     end
   end
   winning_arr.empty? ? random_arr.sample.reverse : winning_arr.flatten.reverse
  end

  def vertical(board)
    arr = []
    board.each_with_index do |row,i|
      arr[i] = []
      board[i].each_with_index do |col,j|
         arr[i] << board[j][i]
      end
    end
    arr
  end

end
