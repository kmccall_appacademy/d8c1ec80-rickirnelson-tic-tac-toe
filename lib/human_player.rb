class HumanPlayer
  attr_accessor :name, :mark, :board
  def initialize(name)
    @name = name
  end

  def get_move
    puts "where to move -> write coordinates exactly like this: 0, 0"
    #until valid_move?(move)
      move = gets.chomp
      move.split('').select {|n| ("0".."9").to_a.include?(n)}.map(&:to_i)
  end

  def display(board)
    print board.place_mark([0,0],:X)
  end

  # def valid_move?(move)
  #   move = parse_move(move)
  #   return false unless move.length == 2
  #   move.all? {|coord| coord.between?(0,2) }
  # end

  # def parse_move(move)
  #   move.split(",").map(&:to_i)
  # end
end
