require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
# In your Game class, set the marks of the players you are passed. Include the following methods
attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = @player_one
  end

  def board
    @board
  end

  def play_turn
    @current_player.display(@board)
    move = current_player.get_move
    @board.place_mark(move,@current_player.mark)
    switch_players! #w/outself
  end

  def play
    until @board.over?
      play_turn
    end
    # conclude
  end

  # def current_player(player=@player_one)
  #   player
  # end

  def switch_players!
  if @current_player == @player_one
    @current_player = @player_two
  else
    @current_player = @player_one
  end
  #   if current_player == @player_one
  #     current_player(@player_two)
  #   else
  #     current_player(@player_one)
  #   end
  end
  #def conclude
    #puts
    #puts @
end
